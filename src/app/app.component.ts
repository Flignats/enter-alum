import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IPlayer } from './models/player.model';
import { IGame } from './models/game.model';
import * as fromCore from './core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  players$: Observable<IPlayer[]>;
  gameDetails$: Observable<IGame[]>;

  activePlayer: string;

  constructor(private afs: fromCore.FirestoreService) {
    this.getPlayers();
  }

  ngOnInit() {
    console.log(' /**', '\n', ' * Name: Ray Williams', '\n', ' * Date: 2019-08-25', '\n', ' */');
  }

  public onSelectPlayer(uid)  {
    this.activePlayer = uid;

    return this.gameDetails$ = this.afs.col$('games', ref =>
      ref.where('players', 'array-contains', uid)
    );
  }

  private getPlayers() {
    return this.players$ = this.afs.col$('users');
  }
}

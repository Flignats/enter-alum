import { Timestamp } from '@firebase/firestore-types';

export interface IPlayer {
    banned: boolean;
    createdAt: Timestamp;
    firstName: string;
    gamesPlayed: number;
    gamesWon: number;
    lastName: string;
    uid: string;
    updatedAt: Timestamp;
}

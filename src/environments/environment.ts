// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyA5RFXo81ivKyTrGmQxZlS7L1FZZooXrsc',
    authDomain: 'enter-alum.firebaseapp.com',
    databaseURL: 'https://enter-alum.firebaseio.com',
    projectId: 'enter-alum',
    storageBucket: '',
    messagingSenderId: '1008358130050',
    appId: '1:1008358130050:web:a71bf99dc54730da'
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

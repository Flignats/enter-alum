import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { IPlayer } from 'src/app/models/player.model';

@Component({
  selector: 'app-player-info',
  templateUrl: './player-info.component.html',
  styleUrls: ['./player-info.component.scss']
})
export class PlayerInfoComponent {

  constructor(
    public dialogRef: MatDialogRef<PlayerInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IPlayer
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

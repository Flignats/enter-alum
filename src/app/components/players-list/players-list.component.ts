import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IPlayer } from 'src/app/models/player.model';
import { IGame } from 'src/app/models/game.model';
import { MatDialog } from '@angular/material/dialog';
import { PlayerInfoComponent } from 'src/app/shared/components/player-info/player-info.component';

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss']
})
export class PlayersListComponent {
  @Input() activePlayer: string;
  @Input() gameDetails: IGame[];
  @Input() players: IPlayer[];

  @Output() selectPlayer = new EventEmitter<any>();

  playersDisplayedColumns: string[] = ['name', 'stats', 'select'];
  gameDetailsDisplayedColumns: string[] = ['winner', 'loser'];

  getLoserName(game: IGame) {
    if (!game) { return; }
    const loser = this.getPlayer(this.players, game.loserId);

    return loser.firstName + ' ' + loser.lastName;
  }
  getWinnerName(game: IGame) {
    if (!game) { return; }
    const winner = this.getPlayer(this.players, game.winnerId);

    return winner.firstName + ' ' + winner.lastName;
  }
  isPlayerActive(player: IPlayer) {
    if (!player) { return false; }

    return this.activePlayer && this.activePlayer === player.uid;
  }
  onSelectPlayer(uid: string) {
    this.selectPlayer.emit(uid);
  }
  openDialog(player): void {
    this.dialog.open(PlayerInfoComponent, {
      width: '450px',
      data: player
    });
  }
  openLoserDialog(game): void {
    const loser = this.getPlayer(this.players, game.loserId);

    this.openDialog(loser);
  }
  openWinnerDialog(game): void {
    const winner = this.getPlayer(this.players, game.winnerId);

    this.openDialog(winner);
  }

  private getPlayer(players, playerId) {
    if (!players || !playerId) { return; }

    return players.find(player => player.uid === playerId);
  }

  constructor(public dialog: MatDialog) { }
}

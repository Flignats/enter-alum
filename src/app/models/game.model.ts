export interface IGame {
    gameId: string;
    loserId: string;
    loserScore: number;
    players: string[];
    winnerId: string;
    winnerScore: number;
}
